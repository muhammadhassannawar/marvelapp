//
//  HomeResponse.swift
//  MoviesList
//
//  Created by Mohamed Hassan Nawar on 07/01/2023.
//

import Foundation

// MARK: - HomeItemRepresentable Protocol
protocol HomeItemRepresentable {
    var itemID: Int? { get }
    var itemTitle: String? { get }
    var itemSubTitle: String? { get }
    var itemImageName: String? { get }
}

// MARK: - HomeResponse
struct HomeResponse: Codable {
    let code: Int
    let data: HomeItemResponse
}

// MARK: - HomeItemResponse
struct HomeItemResponse: Codable {
    let offset: Int
    let results: [HomeItem]
}

// MARK: - HomeItem
struct HomeItem: Codable, Identifiable {
    var id: Int?
    var title, name, description: String?
    var thumbnail: Thumbnail?
}

// MARK: - Thumbnail
struct Thumbnail: Codable {
    let path: String?
    let thumbnailExtension: String?
    
    enum CodingKeys: String, CodingKey {
        case path
        case thumbnailExtension = "extension"
    }
}

// MARK: - HomeItem + HomeItemRepresentable
extension HomeItem: HomeItemRepresentable {
    var itemID: Int? {
        return id.value
    }
    var itemSubTitle: String? {
        return description
    }
    var itemTitle: String? {
        return title ?? name
    }
    var itemImageName: String? {
        guard let first = thumbnail?.path,
              let last = thumbnail?.thumbnailExtension else { return nil}
        return first + .dot + last
    }
}
