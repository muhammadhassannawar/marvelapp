//
//  HomeView.swift
//  MoviesList
//
//  Created by Mohamed Hassan Nawar on 07/01/2023.
//

import SwiftUI
import SDWebImageSwiftUI

struct HomeView: View {
    // MARK: - ROUTER
    @Environment(\.localStatusBarStyle) var statusBarStyle
    let router: MainRouter = .shared
    
    // MARK: - PROPERTIES
    @State var navigationTintColor: Color = .white
    @StateObject var viewModel: HomeViewModel = .init()
    
    // MARK: - BODY
    var body: some View {
        BaseView(
            state: $viewModel.state,
            alertItem: $viewModel.alertItem
        ) {
            ZStack(alignment: .top) {
                ScrollView(.vertical, showsIndicators: false) {
                    VStack(spacing: Dimensions.Home.padding) {
                        carouselImageSliderView
                        charactersView
                        comicsView
                        eventsView
                        seriesView
                        storiesView
                    } //: VStack
                    .padding(.top, Dimensions.Home.topPadding)
                } //: ScrollView
                
                navigationView
            } //: ZStack
            .background(LinearGradient.gradient_01)
            .edgesIgnoringSafeArea(.top)
        } //: BASEVIEW
    }
}

// MARK: - Navigation View
private extension HomeView {
    var navigationView: some View {
        HStack {
            BaseNavigationView(
                leading: navigationLeadingView,
                center: navigationCenterView,
                trailing: navigationTrailingView,
                tintColor: $navigationTintColor
            )
        } //: HStack
        .wrapInHStack(.leading)
        .padding(.bottom, Dimensions.Home.padding)
        .padding(.top, Dimensions.StatusBar.height)
        .background(LinearGradient.gradient_01)
    }
    
    var navigationLeadingView: AnyView {
        Image.Home.marvelLogo
            .resizable()
            .frame(width: Dimensions.Home.Navigation.navigationLogoSize.width, height:  Dimensions.Home.Navigation.navigationLogoSize.height)
            .scaledToFill()
            .wrapInHStack(.leading)
            .eraseToAnyView()
    }
    
    var navigationCenterView: AnyView {
        Text(viewModel.title)
            .eraseToAnyView()
    }
    
    var navigationTrailingView: AnyView {
        Button(action: {
            router.openSearchView()
        }, label: {
            Image.Home.magnifyingGlass
                .foregroundColor(Color.black)
        })
        .wrapInHStack(.trailing).eraseToAnyView()
    }
}

private extension HomeView {
    var carouselImageSliderView: some View {
        CarouselCollectionView(content: imagesList)
            .frame(width: UIScreen.screenWidth)
            .padding(.top, Dimensions.Home.imagesListTopPadding)
    }
    
    var imagesList: [AnyView] {
        viewModel.images().map {item -> AnyView in
            return item.image
                .resizable()
                .scaledToFill()
                .frame(height: Dimensions.Home.imagesListHeight)
                .cornerRadius(Dimensions.Home.imagesListCornerRadius)
                .eraseToAnyView()
        }
    }
}

