//
//  Font.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 06/01/2023.
//

import SwiftUI

extension Font {
    private static let bold = "NotoSansArabicUI-Bold"
    private static let regular = "NotoSansArabicUI-Regular"
    
    enum Heading {
        static let title_1 = Font.custom(bold, fixedSize: 24.0)
    }
    
    enum Body {
        static let body_1 = Font.custom(regular, fixedSize: 16.0)
        static let body_1_highlight = Font.custom(bold, fixedSize: 16.0)
        static let body_2 = Font.custom(regular, fixedSize: 14.0)
    }
    
    enum Supportive {
        static let caption = Font.custom(regular, fixedSize: 12.0)
    }
}
