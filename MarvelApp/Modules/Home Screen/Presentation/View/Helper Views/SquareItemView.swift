//
//  SquareItemView.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 06/01/2023.
//

import SwiftUI
import SDWebImageSwiftUI

struct SquareItemView: View, Initializable {
    // MARK: - Properties
    private let title: String?
    private let subtitle: String?
    private let imageURL: String?
    
    // MARK: - Init
    init(title: String?, imageName: String?, subtitle: String?) {
        self.title = title
        self.subtitle = subtitle
        self.imageURL = imageName
    }
    
    // MARK: - Body
    var body: some View {
        ZStack(alignment: .bottomLeading) {
            WebImage(url: URL(string: imageURL.value))
                .frame(width: Dimensions.Home.SquareItem.imageSize.width, height: Dimensions.Home.SquareItem.imageSize.height)
            
            VStack(alignment: .leading) {
                Spacer()
                addTextIfNeeded(word: title)
                    .foregroundColor(Color.white)
            } //: ZStack
            .padding(Dimensions.Home.SquareItem.textsPadding)
        } //: ZStack
        .frame(
            width: Dimensions.Home.SquareItem.itemSize.width,
            height: Dimensions.Home.SquareItem.itemSize.height,
            alignment: .center
        )
        .background(LinearGradient.gradient_02)
        .cornerRadius(Dimensions.Home.SquareItem.cornerRadius)
        .shadow(radius: Dimensions.Home.SquareItem.shadowRadius)
        .padding(Dimensions.Home.SquareItem.itemPadding)
    }
    
    func addImageIfNeeded() -> AnyView? {
        guard let imageName = imageURL else { return nil }
        return AnyView(
            Image(imageName)
        )
    }
}
