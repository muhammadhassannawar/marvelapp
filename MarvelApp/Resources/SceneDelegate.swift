//
//  SceneDelegate.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import SwiftUI

final class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    private let router: MainRouter = .shared
    static var shared: SceneDelegate {
        guard let delegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate
        else {
            fatalError("Should have a delegate")
        }
        return delegate
    }
    
    var window: UIWindow?
    
    override init() {
        UIScrollView.appearance().bounces = false
        UIScrollView.appearance().showsVerticalScrollIndicator = false
        UIScrollView.appearance().showsHorizontalScrollIndicator = false
        super.init()
    }
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        let window = UIWindow(windowScene: windowScene)
        self.window = window
        router.openHome()
        window.makeKeyAndVisible()
    }
}
