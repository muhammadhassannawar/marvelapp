//
//  CarouselCollectionView.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 08/01/2023.
//

import SwiftUI

struct CarouselCollectionView<carouselContentView: View>: View {
    // MARK: - State Properties
    @State private var offset: CGFloat = .zero
    @State private var selectedIndex: Int
    
    // MARK: - Properties
    private let content: [carouselContentView]
    private let capsuleBackground: AnyView
    private let capsuleWidth: CGFloat
    private let capsuleHeight: CGFloat
    private let capsuleSpacing: CGFloat
    private let selectedCapsuleWidth: CGFloat
    private let contentHeight: CGFloat
    private let contentSpacing: CGFloat
    private var currentIndexCompletion: ((Int) -> Void)?
    
    // MARK: - init
    init(
        content: [carouselContentView],
        capsuleBackground: AnyView = LinearGradient.gradient_02.eraseToAnyView(),
        capsuleWidth: CGFloat = Dimensions.Home.CarouselCollectionView.capsuleWidth,
        capsuleHeight: CGFloat = Dimensions.Home.CarouselCollectionView.capsuleHeight,
        capsuleSpacing: CGFloat = Dimensions.Home.CarouselCollectionView.capsuleSpacing,
        selectedCapsuleWidth: CGFloat = Dimensions.Home.CarouselCollectionView.selectedCapsuleWidth,
        contentHeight: CGFloat = Dimensions.Home.CarouselCollectionView.contentHeight,
        contentSpacing: CGFloat = Dimensions.Home.CarouselCollectionView.contentSpacing,
        allowIndicator: Bool = true,
        selectedIndex: Int = .zero,
        completion: ((Int) -> Void)? = nil
    ) {
        self.content = content
        self.capsuleBackground = capsuleBackground
        self.capsuleWidth = capsuleWidth
        self.capsuleHeight = capsuleHeight
        self.capsuleSpacing = capsuleSpacing
        self.selectedCapsuleWidth = selectedCapsuleWidth
        self.contentHeight = contentHeight
        self.contentSpacing = contentSpacing
        self.selectedIndex = selectedIndex
        self.currentIndexCompletion = completion
    }
    // MARK: - body
    var body: some View {
        if content.isEmpty {
            EmptyView().eraseToAnyView()
        } else if content.count == .zero {
            getContent(at: .zero)
                .frame(width: carouselContentViewWidth, alignment: .leading)
        } else {
            VStack {
                contentsContent
                indicators
            }//: VStack
        }
    }//: body
}

// MARK: - private Views
private extension CarouselCollectionView {
    var contentsContent: some View {
        HStack(
            alignment: .center,
            spacing: contentSpacing
        ) {
            ForEach(content.indices, id: \.self) { index in
                getContent(at: index)
                    .frame(width: carouselContentViewWidth,
                           alignment: .leading)
            }//: ForEach
        }//: HStack
        .modifier(
            ScrollviewCarouselModifier(
                itemsCount: content.count,
                itemWidth: carouselContentViewWidth,
                itemSpacing: contentSpacing,
                selectedIndex: selectedIndex
            )//: ScrollviewCarouselModifier
        )//: modifier
        .padding(.vertical, 4)
    }
    
    func getContent(at index: Int) -> AnyView {
        guard index == .zero else {
            return content[index]
                .eraseToAnyView()
        }
        return content[index]
            .overlay(
                GeometryReader { proxy -> Color in
                    let minX = proxy.frame(in: .global).minX
                    updateOffset(with: minX)
                    return Color.clear
                }//: GeometryReader
                    .frame(width: .zero, height: .zero)
                ,
                alignment: .leading
            )//: overlay
            .eraseToAnyView()
    }//: getTabContent
    
    var indicators: some View {
        HStack(alignment: .bottom, spacing: capsuleSpacing) {
            ForEach(content.indices, id: \.self) { index in
                let width = currentIndex == index ? selectedCapsuleWidth : capsuleWidth
                getCapsule(
                    background: capsuleBackground,
                    width: width,
                    height: capsuleHeight
                )//: getCapsule
            }//: ForEach
        }//: HStack
        .overlay(
            getCapsule(
                background: capsuleBackground,
                width: selectedCapsuleWidth,
                height: capsuleHeight
            )//: getCapsule
                .offset(x: currentOffset)
            ,
            alignment: .leading
        )//: overlay
        .padding(.bottom, Dimensions.Home.CarouselCollectionView.bottomPadding)
        .eraseToAnyView()
    }
    
    func getCapsule(
        background: AnyView,
        width: CGFloat,
        height: CGFloat,
        alignment: Alignment = .leading
    ) -> some View {
        Capsule()
            .fill(Color.clear)
            .background(background)
            .frame(width: width, height: height)
            .frame(alignment: alignment)
            .cornerRadius(height / 2)
    }//: getCapsule
    
}

// MARK: - private Helper Views
private extension CarouselCollectionView {
    var carouselContentViewWidth: CGFloat {
        Dimensions.Home.CarouselCollectionView.contentWidth
    }
    
    var currentOffset: CGFloat {
        (capsuleSpacing + capsuleWidth) * carouselContentViewProgress
    }
    
    var currentIndex: Int {
        let currentIndex = Int(round(Double(carouselContentViewProgress)))
        return currentIndex
    }
    
    var carouselContentViewProgress: CGFloat {
        offset / carouselContentViewWidth
    }
    
    func updateOffset(with minX: CGFloat) {
        DispatchQueue.main.async {
            withAnimation(Animation.linear) {
                let xValue = -minX
                self.offset = xValue + (UIScreen.screenWidth - carouselContentViewWidth) + contentSpacing
            }//: withAnimation
            updateCurrentIndex()
        }//: DispatchQueue
    }//: updateOffset
    
    func updateCurrentIndex() {
        if currentIndex != selectedIndex {
            selectedIndex = currentIndex
            currentIndexCompletion?(selectedIndex)
        }
    }
}
