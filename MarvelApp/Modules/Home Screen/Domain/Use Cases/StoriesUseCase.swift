//
//  StoriesUseCase.swift
//  MoviesList
//
//  Created by Mohamed Hassan Nawar on 07/01/2023.
//

import Combine

final class StoriesUseCase: StoriesUseCaseContract {
    // MARK: - PROPERTIES
    let repo: HomeRepositoryContract
    
    // MARK: - INIT
    init(repo: HomeRepositoryContract = HomeRepository()) {
        self.repo = repo
    }
    
    // MARK: - METHODS
    func execute(searchKey: String?, offset: Int?, completionBlock: @escaping (HomeResponse?, String?) -> Void) {
        var parameter = [Localizable.API.offset: offset.value] as [String: AnyObject]
        if searchKey.value.isNotEmpty {
            parameter.updateValue(searchKey.value as AnyObject, forKey: Localizable.API.nameStartsWith)
        }
        return repo.loadStories(using: parameter, completionBlock: completionBlock)
    }
}
