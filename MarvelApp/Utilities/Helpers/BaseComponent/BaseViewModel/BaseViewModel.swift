//
//  BaseViewModel.swift
//  FootballCompetitions
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import Foundation

class BaseViewModel: DisposeObject {
    // MARK: - PROPERTIES
    @Published var state: ViewModelState = .idle
    @Published var alertItem: AlertItem?
}
