//
//  BaseBottomSheet.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 06/01/2023.
//

import SwiftUI

struct BaseBottomSheet<Content: View>: View {
    @Environment(\.safeAreaInsets) private var safeAreaInsets
    
    private let content: Content
    private let opacity: Double
    private let title: String?
    private let titleFont: Font
    private let titleColor: Color
    private let insets: UIEdgeInsets
    private let closeAction: (() -> Void)?
    private let isDismissible: Bool
    
    init(
        opacity: Double = .zero,
        title: String? = nil,
        titleFont: Font = Font.Heading.title_1,
        titleColor: Color = Color.Core.text_01,
        insets: UIEdgeInsets = Dimensions.BaseBottomSheet.insets,
        closeAction: (() -> Void)? = nil,
        @ViewBuilder content: () -> Content,
        isDismissible: Bool = false
    ) {
        self.opacity = opacity
        self.title = title
        self.titleFont = titleFont
        self.titleColor = titleColor
        self.insets = insets
        self.closeAction = closeAction
        self.content = content()
        self.isDismissible = isDismissible
    }
    
    var body: some View {
        VStack {
            Spacer()
            VStack(spacing: insets.top) {
                headerView
                content
            }
            .frame(maxWidth: .infinity)
            .padding(.top, insets.top)
            .padding(.horizontal, insets.left)
            .padding(.bottom, insets.bottom + safeAreaInsets.bottom)
            .background(Color.Core.text_04)
            .clipShape(
                RoundedCorner(
                    radius: 16.0,
                    corners: [.topLeft, .topRight]
                )
            )
        }
        .scrollIfNeeded()
        .edgesIgnoringSafeArea(.bottom)
        .overlay(
            when: opacity > .zero,
            using: Color.Core.text_01.opacity(opacity)
        )
        .onTapGesture {
            if isDismissible { closeAction?() }
        }
    }
    
    private var headerView: AnyView? {
        guard let title = title,
              let action = closeAction
        else { return nil }
        
        return BaseBottomSheetHeader(
            title: title,
            titleFont: titleFont,
            titleColor: titleColor,
            action: action
        )
        .eraseToAnyView()
    }
}
