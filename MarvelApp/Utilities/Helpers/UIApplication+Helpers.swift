//
//  UIApplication+Helpers.swift
//  FootballCompetitions
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import UIKit
import SwiftUI

extension UIApplication {
    // MARK: - Top ViewController
    var topController: UIViewController? {
        let keyWindow = UIApplication.shared.windows.first(where: { $0.isKeyWindow })
        guard var topController = keyWindow?.rootViewController else { return nil }
        
        while let presentedViewController = topController.presentedViewController {
            topController = presentedViewController
        }
        
        return topController
    }
    
    var window: UIWindow? {
        UIApplication.shared.windows.first(where: { $0.isKeyWindow })
    }
}

extension UIApplication {
    // MARK: - AlertView
    func showAlertView(
        _ alertData: AlertItem,
        completion: @escaping () -> Void
    ) {
        guard let topController = topController,
              !(topController is UIAlertController)
        else {
            return
        }
        
        let alert = UIAlertController(
            title: alertData.title.value,
            message: alertData.message.value,
            preferredStyle: alertData.style
        )
        for action in alertData.actions {
            alert.addAction(action.toUIAlertAction)
        }
        topController.present(alert, animated: true, completion: completion)
    }
}
