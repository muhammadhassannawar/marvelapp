//
//  ScrollviewCarouselModifier.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 08/01/2023.
//

import SwiftUI

struct ScrollviewCarouselModifier: ViewModifier {
    // MARK: - PROPERTIES
    @State private var scrollOffset: CGFloat
    @State private var dragOffset: CGFloat
    private var itemsCount: CGFloat
    private var itemWidth: CGFloat
    private var itemSpacing: CGFloat
    private var selectedIndex: Int
    
    // MARK: - INIT
    init(itemsCount: Int, itemWidth: CGFloat, itemSpacing: CGFloat, selectedIndex: Int) {
        self.itemsCount = CGFloat(itemsCount)
        self.itemWidth = itemWidth
        self.itemSpacing = itemSpacing
        self.selectedIndex = (selectedIndex % itemsCount - 1)
        
        // Calculate Total Content Width
        let contentWidth: CGFloat = self.itemsCount * itemWidth + (self.itemsCount - 1) * itemSpacing
        let screenWidth = UIScreen.main.bounds.width
        
        // Set Initial Offset to first Item
        let initialOffset: CGFloat = (contentWidth / 2.0) - (screenWidth / 2.0) + ((screenWidth - itemWidth) / 2.0) - (itemWidth * CGFloat(selectedIndex))
        
        self._scrollOffset = State(initialValue: CGFloat(initialOffset))
        self._dragOffset = State(initialValue: 0)
    }
    
    // MARK: - BODY
    func body(content: Content) -> some View {
        content
            .offset(x: scrollOffset + dragOffset, y: 0)
            .gesture(DragGesture()
                .onChanged(didChangeDrag)
                .onEnded(didEndDrag)
            )
    }
    
    private func didChangeDrag(_ event: DragGesture.Value) {
        dragOffset = event.translation.width
    }
    
    private func didEndDrag(_ event: DragGesture.Value) {
        // Scroll to where user dragged
        
        scrollOffset += event.translation.width
        dragOffset = 0
        
        // Now calculate which item to snap to
        let contentWidth: CGFloat = itemsCount * itemWidth + (itemsCount - 1) * itemSpacing
        let screenWidth = UIScreen.main.bounds.width
        
        // Center position of current offset
        let center = scrollOffset + (screenWidth / 2.0) + (contentWidth / 2.0)
        
        // Calculate which item we are closest to using the defined size
        var index = (center - (screenWidth / 2.0)) / (itemWidth + itemSpacing)
        
        // Should we stay at current index or are we closer to the next item...
        index = index.remainder(dividingBy: 1) > 0.5 ? index + 1 : CGFloat(Int(index))
        
        // Protect from scrolling out of bounds
        index = min(index, itemsCount - 1)
        index = max(index, 0)
        
        // Set final offset (snapping to item)
        let newOffset = index * itemWidth + (index - 1) * itemSpacing - (contentWidth / 2.0) + (screenWidth / 2.0) - ((screenWidth - itemWidth) / 2.0) + itemSpacing
        
        // Animate snapping
        withAnimation(Animation.linear) {
            scrollOffset = newOffset
        }
    }
}
