//
//  LinearGradient+Assets.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 06/01/2023.
//

import SwiftUI

extension LinearGradient {
    static let gradient_01 = LinearGradient(gradient: .gradient_01, startPoint: .leading, endPoint: .trailing)
    static let gradient_02 = LinearGradient(gradient: .gradient_02, startPoint: .leading, endPoint: .trailing)
}
