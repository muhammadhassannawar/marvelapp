//
//  OverflowViewModifier.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 06/01/2023.
//

import SwiftUI

struct OverflowViewModifier: ViewModifier {
    // MARK: - PROPERTIES
    @State private var hasReachedMaximumValue: Bool = false
    
    // MARK: - BODY
    func body(content: Content) -> some View {
        GeometryReader { geometry in
            content
                .background(
                    GeometryReader { contentGeometry in
                        Color.clear
                            .onAppear {
                                hasReachedMaximumValue = contentGeometry.size.height > geometry.size.height
                            }
                    }
                )
                .wrapInScrollView(
                    when: hasReachedMaximumValue
                )
        } //: GeometryReader
    }
}

extension View {
    @ViewBuilder
    func wrapInScrollView(when condition: Bool) -> some View {
        if condition {
            ScrollView(.vertical, showsIndicators: false) {
                self
            }
        } else {
            self
        }
    }
}

extension View {
    func scrollIfNeeded() -> some View {
        modifier(OverflowViewModifier())
    }
}
