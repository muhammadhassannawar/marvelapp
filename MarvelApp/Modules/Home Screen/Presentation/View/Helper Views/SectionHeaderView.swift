//
//  SectionHeaderView.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 06/01/2023.
//

import SwiftUI

struct SectionHeaderView: View {
    // MARK: - Properties
    private var title: String
    private var description: String?
    private var buttonData: ButtonAction?
    
    // MARK: - Init
    init(title: String, description: String?, buttonData: ButtonAction?) {
        self.title = title
        self.description = description
        self.buttonData = buttonData
    }
    
    // MARK: - Body
    var body: some View {
        HStack(alignment: .top) {
            VStack(alignment: .leading, spacing: Dimensions.SectionHeaderView.spacing) {
                addTextIfNeeded(word: title)?
                    .font(Font.Body.body_1_highlight)
                addTextIfNeeded(word: description)
                    .font(Font.Body.body_2)
                    .foregroundColor(Color.Core.text_02)
            } //: VStack
            Spacer()
            addButtonIfNeeded()
        } //: HStack
    }
    
    func addButtonIfNeeded() -> AnyView? {
        guard let buttonData = buttonData else { return nil }
        
        return Button(action: buttonData.action, label: {
            Text(buttonData.title)
                .font(Font.Body.body_2)
                .foregroundColor(Color.Core.text_02)
        })
        .eraseToAnyView()
    }
}
