//
//  RectangleItemView.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 06/01/2023.
//

import SwiftUI
import SDWebImageSwiftUI

struct RectangleItemView: View, Initializable {
    // MARK: - PROPERTIES
    private let title: String?
    private let subtitle: String?
    private let imageName: String?
    
    // MARK: - INIT
    init(title: String? = nil, imageName: String? = nil, subtitle: String? = nil) {
        self.title = title
        self.subtitle = subtitle
        self.imageName = imageName
    }
    
    // MARK: - BODY
    var body: some View {
        ZStack {
            WebImage(url: URL(string: imageName.value))
                .scaledToFill()
                .frame(
                    width: Dimensions.RectangleItemView.size.width,
                    height: Dimensions.RectangleItemView.size.height,
                    alignment: .center
                )
                .background(Color.green)
            
            addTextIfNeeded(word: title)?
                .font(Font.Body.body_1_highlight)
                .foregroundColor(Color.Core.text_04)
                .frame(
                    width: Dimensions.RectangleItemView.size.width)
        } //: ZStack
        .cornerRadius(Dimensions.RectangleItemView.cornerRadius)
    }
}
