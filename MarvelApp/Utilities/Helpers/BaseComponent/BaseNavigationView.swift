//
//  BaseNavigationView.swift
//  FootballCompetitions
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import SwiftUI

struct BaseNavigationView: View {
    // MARK: - Properties
    let trailingView: AnyView?
    let leadingView: AnyView?
    let centerView: AnyView?
    
    // MARK: - Bindings
    @Binding var tintColor: Color
    
    // MARK: - Init
    init(
        leading: AnyView? = nil,
        center: AnyView? = nil,
        trailing: AnyView? = nil,
        tintColor: Binding<Color> = .constant(.black)
    ) {
        self._tintColor = tintColor
        self.trailingView = trailing
        self.leadingView = leading
        self.centerView = center
    }
    
    // MARK: - Body
    var body: some View {
        HStack(
            alignment: .center,
            spacing: Dimensions.NavigationView.spacing
        ) {
            leadingView
                .foregroundColor(tintColor)
            centerView
            trailingView
                .foregroundColor(tintColor)
        } //: HStack
        .padding(.horizontal, Dimensions.NavigationView.horizontalPadding)
    }
}
