//
//  ViewModelState.swift
//  FootballCompetitions
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import Foundation

enum ViewModelState {
    case idle
    case loading
    case successful
    case failed(message: String)
}

extension ViewModelState: Equatable {
    static func == (lhs: ViewModelState, rhs: ViewModelState) -> Bool {
        switch (lhs, rhs) {
        case (.idle, .idle): return true
        case (.loading, .loading): return true
        case (.successful, .successful): return true
            
        case let (.failed(lhsError), .failed(rhsError)):
            guard let lhsError = lhsError as String?,
                  let rhsError = rhsError as String? else {
                return false
            }
            return lhsError == rhsError
            
        default:
            return false
        }
    }
}

extension ViewModelState {
    var alertItem: AlertItem? {
        guard case .failed(let error) = self
        else { return nil }
        let alertActions: [AlertAction] = [.init(title: Localizable.General.okay, action: {})]
        return AlertItem(title: Localizable.General.error, message: error, actions: alertActions)
    }
}

public enum ValidationState: Equatable {
    case none
    case success
    case error(message: String)
}

extension ViewModelState {
    var validationState: ValidationState {
        switch self {
        case .successful:
            return .success
            
        case .failed(let error):
            return .error(message: error)
            
        default: return .none
        }
    }
}
