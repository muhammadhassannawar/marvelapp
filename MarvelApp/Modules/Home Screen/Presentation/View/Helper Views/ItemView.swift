//
//  ItemView.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 06/01/2023.
//

import SwiftUI
import SDWebImageSwiftUI

struct ItemView: View, Initializable {
    // MARK: - Properties
    private let title: String?
    private let subtitle: String?
    private let imageName: String?
    
    // MARK: - Init
    init(title: String? = nil, imageName: String? = nil, subtitle: String? = nil) {
        self.title = title
        self.subtitle = subtitle
        self.imageName = imageName
    }
    
    // MARK: - BODY
    var body: some View {
        ZStack(alignment: .bottom) {
            WebImage(url: URL(string: imageName.value))
                .scaledToFill()
                .frame(
                    width: Dimensions.ItemView.size.width,
                    height: Dimensions.ItemView.size.height,
                    alignment: .center
                )
            
            HStack {
                VStack(alignment: .center) {
                    addTextIfNeeded(word: title)?
                        .font(Font.Body.body_1_highlight)
                        .fontWeight(.bold)
                        .foregroundColor(Color.Core.text_04)
                    
                    addTextIfNeeded(word: subtitle)?
                        .font(Font.Body.body_2)
                        .foregroundColor(Color.Core.text_04)
                        .lineLimit(1)
                } //: VStack
                
                Spacer()
            } //: HStack
            .padding()
        } //: ZStack
        .cornerRadius(Dimensions.ItemView.cornerRadius)
    }
}
