//
//  HomeView+Extension.swift
//  MoviesList
//
//  Created by Mohamed Hassan Nawar on 07/01/2023.
//

import SwiftUI

// MARK: - Home + Characters View
extension HomeView {
    var charactersHeaderData: HeaderModel {
        HeaderModel(title: Localizable.Home.Characters.title, buttonData: ButtonAction(title: Localizable.Home.seeAll,                                                                   action: {
            router.openSearchView(type: .characters)
        }))
    }
    var charactersView: some View {
        return HomeSectionView<HomeItem>(
            items: viewModel.characters, style: .horizontalRectangle,
            headerData: charactersHeaderData,
            onSelect: {
                router.openDetails(item: $0)
            }).eraseToAnyView()
    }
}


// MARK: - Home + Comics View
extension HomeView {
    var comicsHeaderData: HeaderModel {
        HeaderModel(title: Localizable.Home.Comics.title, buttonData: ButtonAction(title: Localizable.Home.seeAll,                                                                   action: {
            router.openSearchView(type: .comics)
        }))
    }
    var comicsView: some View {
        return HomeSectionView<HomeItem>(
            items: viewModel.comics, style: .verticalRectangle,
            headerData: comicsHeaderData,
            onSelect: {
                router.openDetails(item: $0)
            }).eraseToAnyView()
    }
}


// MARK: - Home + Stories View
extension HomeView {
    var storiesHeaderData: HeaderModel {
        HeaderModel(title: Localizable.Home.Stories.title, buttonData: ButtonAction(title: Localizable.Home.seeAll,                                                                   action: {
            router.openSearchView(type: .stories)
        }))
    }
    var storiesView: some View {
        return HomeSectionView<HomeItem>(
            items: viewModel.stories, style: .circle,
            headerData: storiesHeaderData,
            onSelect: {
                router.openDetails(item: $0)
            }).eraseToAnyView()
    }
}


// MARK: - Home + Events View
extension HomeView {
    var eventsHeaderData: HeaderModel {
        HeaderModel(title: Localizable.Home.Events.title, buttonData: ButtonAction(title: Localizable.Home.seeAll,                                                                   action: {
            router.openSearchView(type: .events)
        }))
    }
    var eventsView: some View {
        return HomeSectionView<HomeItem>(
            items: viewModel.events, style: .square,
            headerData: eventsHeaderData,
            onSelect: {
                router.openDetails(item: $0)
            }).eraseToAnyView()
    }
}


// MARK: - Home + Series View
extension HomeView {
    var seriesHeaderData: HeaderModel {
        HeaderModel(title: Localizable.Home.Series.title, buttonData: ButtonAction(title: Localizable.Home.seeAll,                                                                   action: {
            router.openSearchView(type: .series)
        }))
    }
    var seriesView: some View {
        return HomeSectionView<HomeItem>(
            items: viewModel.series, style: .square,
            headerData: seriesHeaderData,
            onSelect: {
                router.openDetails(item: $0)
            }).eraseToAnyView()
    }
}
