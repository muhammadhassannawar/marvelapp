//
//  String+Helpers.swift
//  FootballCompetitions
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

extension Optional where Wrapped == String {
    var value: String {
        return self ?? ""
    }
}

extension String {
    static var empty: String {
        return ""
    }
}

extension String {
    var isNotEmpty: Bool {
        return !self.isEmpty
    }
}

extension String {
    static var dot: String {
        return "."
    }
    
    static var space: String {
        return " "
    }
}
