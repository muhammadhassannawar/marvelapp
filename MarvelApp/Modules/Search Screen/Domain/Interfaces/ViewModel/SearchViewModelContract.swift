//
//  SearchViewModelContract.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 15/01/2023.
//

import Combine

typealias SearchViewModelContract = BaseViewModel & ObservableObject & SearchViewModelInput & SearchViewModelOutput

protocol SearchViewModelInput {
    func resetState()
    func didPressSearch()
    func didSelectType(using type: SearchItemType?, offset: Int)
    func loadNextPage()
}

protocol SearchViewModelOutput {
    var searchItems: [HomeItem] { get }
    var searchState: ViewModelState { get }
    var shouldShowTypesSheet: Bool { get set }
    var scrollToItemSubject: PassthroughSubject<Int, Never> { get }
    var searchText: String { get }
    var type: SearchItemTypes? { get }
    var navigationTitle: String { get }
    var searchPlaceholder: String { get }
}
