//
//  SelectableItemView.swift
//  MoviesList
//
//  Created by Mohamed Hassan Nawar on 01/06/2022.
//

import SwiftUI

struct SelectableItemView: View {
    enum InputState {
        case focus, idle
    }
    
    typealias OnTap = () -> Void
    
    // MARK: - PROPERTIES
    var placeholder: Binding<String>?
    @Binding var text: String?
    
    let title: String?
    let icon: Image
    let onTap: OnTap
    
    // MARK: - INIT
    init(
        title: String? = nil,
        placeholder: Binding<String>? = nil,
        icon: Image = Image.Arrows.chevron_right,
        text: Binding<String?>,
        onPress: @escaping OnTap
    ) {
        self.title = title
        self.placeholder = placeholder
        self.icon = icon
        self._text = text
        self.onTap = onPress
    }
    
    // MARK: - BODY
    var body: some View {
        HStack(
            alignment: .center,
            spacing: Dimensions.SelectableItemView.stackSpacing
        ) {
            VStack(
                alignment: .leading,
                spacing: Dimensions.SelectableItemView.verticalStackSpacing
            ) {
                titleView
                placeHolderView
                textView
            } //: VStack
            Spacer()
            icon
        } //: HStack
        .foregroundColor(.black)
        .padding(Dimensions.SelectableItemView.padding)
        .frame(
            height: Dimensions.SelectableItemView.frameHeight
        )
        .background(
            RoundedRectangle(cornerRadius: Dimensions.SelectableItemView.cornerRadius).fill(LinearGradient.gradient_01)
        )
        .onTapGesture {
            onTap()
        } //: TapGesture
    }
}

extension SelectableItemView {
    var titleView: AnyView? {
        guard text?.isNotEmpty == true else { return nil }
        var titleText: String?
        if let title = title {
            titleText = title
        } else if let placeholder = placeholder?.wrappedValue,
                  let text = text, text.isNotEmpty {
            titleText = placeholder
        } else {
            titleText = nil
        }
        
        guard let titleText = titleText else { return nil }
        return Text(titleText)
            .font(Font.Supportive.caption)
            .lineLimit(1)
            .frame(height: Dimensions.SelectableItemView.titleHeight)
            .eraseToAnyView()
    }
    
    var placeHolderView: AnyView? {
        guard let placeHolder: String = placeholder?.wrappedValue,
              (text == nil || (text.value).isEmpty)
        else { return nil }
        
        return Text(placeHolder)
            .font(Font.Body.body_1)
            .lineLimit(1)
            .frame(height: Dimensions.SelectableItemView.titleHeight)
            .eraseToAnyView()
    }
    
    var textView: AnyView? {
        guard let text = text, text.isNotEmpty else { return nil }
        return Text(text)
            .font(Font.Body.body_1)
            .lineLimit(1)
            .eraseToAnyView()
    }
}
