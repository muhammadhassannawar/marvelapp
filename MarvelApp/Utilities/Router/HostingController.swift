//
//  HostingController.swift
//  FootballCompetitions
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import SwiftUI

// MARK: - HostingController
//
/// Custom UIHostingController to override preferredStatusBarStyle
///
final class HostingController<Content>: UIHostingController<Content> where Content: View {
    private var internalStyle = UIStatusBarStyle.default
    @objc override dynamic var preferredStatusBarStyle: UIStatusBarStyle {
        get { internalStyle }
        set { internalStyle = newValue
            self.setNeedsStatusBarAppearanceUpdate() }
    }
    override init(rootView: Content) {
        super.init(rootView: rootView)
        
        LocalStatusBarStyleKey.defaultValue.getter = { self.preferredStatusBarStyle }
        LocalStatusBarStyleKey.defaultValue.setter = { self.preferredStatusBarStyle = $0 }
    }
    
    @available(*, unavailable)
    @objc
    required dynamic init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

final class LocalStatusBarStyle {
    var getter: () -> UIStatusBarStyle = { .default }
    var setter: (UIStatusBarStyle) -> Void = { _ in }
    
    var currentStyle: UIStatusBarStyle {
        get { self.getter() }
        set { self.setter(newValue) }
    }
}

/// This is EnvironmentKey used  to change status bar color
///
struct LocalStatusBarStyleKey: EnvironmentKey {
    static let defaultValue: LocalStatusBarStyle = LocalStatusBarStyle()
}

// MARK: - EnvironmentValues + localStatusBarStyle
extension EnvironmentValues {
    var localStatusBarStyle: LocalStatusBarStyle {
        return self[LocalStatusBarStyleKey.self]
    }
}
