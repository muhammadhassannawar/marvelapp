//
//  SearchNavigationView.swift
//  MoviesList
//
//  Created by Mohamed Hassan Nawar on 31/05/2022.
//

import SwiftUI

struct SearchNavigationView: View {
    typealias Completion = (() -> Void)?
    // MARK: - ENVIRONMENTS
    @Environment(\.presentationStyle) var presentationStyle
    
    // MARK: - ROUTER
    let router: MainRouter = .shared
    
    // MARK: - STATES
    @State private var isTyping: Bool = false
    
    // MARK: - BINDINGS
    @Binding private var searchText: String
    
    // MARK: - PROPERTIES
    private let title: String?
    private let resultView: AnyView?
    private let action: Completion
    private let onCommit: Completion
    private let onEditing: Completion
    
    // MARK: - INIT
    init(
        title: String?,
        searchText: Binding<String> = .constant(.empty),
        resultView: AnyView? = nil,
        action: Completion = nil,
        onCommit: Completion = nil,
        onEditing: Completion = nil
    ) {
        self._searchText = searchText
        self.title = title
        self.resultView = resultView
        self.action = action
        self.onCommit = onCommit
        self.onEditing = onEditing
    }
    
    // MARK: - BODY
    var body: some View {
        VStack(spacing: .zero) {
            ZStack(alignment: .bottom) {
                ZStack(alignment: .center) {
                    backgroundView
                        .edgesIgnoringSafeArea(.top)
                    
                    contentView
                        .frame(
                            maxWidth: .infinity,
                            maxHeight: .infinity,
                            alignment: .topLeading
                        )
                        .padding(Dimensions.SearchNavigationView.padding)
                } //: ZStack
            } //: ZStack
            .frame(
                minHeight: Dimensions.SearchNavigationView.navigationViewSize.width,
                maxHeight: Dimensions.SearchNavigationView.navigationViewSize.height
            )
            .fixedSize(
                horizontal: false,
                vertical: true
            )
            
            searchView
        } //: VStack
    }
}


extension SearchNavigationView {
    
    private var backButton: AnyView {
        let pushButton = NavigationButton(action: actionCallback, image: Image.Arrows.arrow_left, renderingMode: .template).eraseToAnyView()
        let closeButton = NavigationButton(action: actionCallback, image: Image.Arrows.cross_close, renderingMode: .template).eraseToAnyView()
        
        return presentationStyle == .push ?  pushButton : closeButton
    }
    
    func actionCallback() {
        guard let completion = action else {
            router.close(using: presentationStyle)
            return
        }
        completion()
    }
    
    private var contentView: some View {
        VStack(
            alignment: .leading,
            spacing: Dimensions.SearchNavigationView.contentSpacing
        ) {
            backButton
            
            Text(title.value)
                .font(Font.Heading.title_1)
                .multilineTextAlignment(.leading)
                .lineLimit(1)
            
            resultView
        } //: VStack
        .foregroundColor(.black)
    }
    private var backgroundView: some View {
        LinearGradient.gradient_01
    }
    
    private var searchView: AnyView? {
        return ZStack {
            Color
                .white
            
            RoundedRectangle(cornerRadius: Dimensions.SearchNavigationView.SearchView.cornerRadius)
                .fill(Color.Core.text_04)
                .frame(height: Dimensions.SearchNavigationView.SearchView.SearchViewHeight)
                .overlay(textfieldView(using: Localizable.General.search))
                .offset(y: Dimensions.SearchNavigationView.SearchView.offsetY)
                .padding(.horizontal)
                .shadow(
                    color: Color.shadow_color,
                    radius: Constants.shadowRadius,
                    x: Constants.shadowX,
                    y: Constants.shadowY
                )
        } //: ZStack
        .frame(height: Dimensions.SearchNavigationView.SearchView.containerHeight)
        .eraseToAnyView()
    }
    
    
    func textfieldView(using placeholder: String) -> some View {
        HStack {
            searchButton
            
            TextField(
                String.empty,
                text: $searchText,
                onEditingChanged: {
                    guard $0 else { return }
                    isTyping = true
                    onEditing?()
                }, onCommit: {
                    isTyping = false
                    onCommit?()
                }
            )
            .placeholder(placeholderView(for: placeholder), show: searchText.isEmpty)
            .font(Font.Body.body_2)
            .foregroundColor(Color.Core.text_01)
            .frame(maxHeight: .infinity)
            
            clearButton
                .isHidden(!isTyping)
        } //: HStack
        .padding(.horizontal)
    }
    
    private var clearButton: some View {
        Button(action: {
            searchText.removeAll()
        }, label: {
            Image
                .Search.remove
                .resizable()
                .frame(width: Dimensions.SearchNavigationView.SearchView.iconSize.width,
                       height: Dimensions.SearchNavigationView.SearchView.iconSize.height)
                .foregroundColor(.gray)
        } //: HStack
        )
    }
    
    private func placeholderView(for value: String) -> some View {
        Text(value)
            .foregroundColor(Color.Core.text_03)
            .font(Font.Body.body_2)
    }
    
    private var searchButton: some View {
        Button(
            action: {
                isTyping = false
                onCommit?()
            },
            label: {
                Image
                    .Home
                    .magnifyingGlass
                    .resizable()
                    .frame(width: Dimensions.SearchNavigationView.SearchView.iconSize.width,
                           height: Dimensions.SearchNavigationView.SearchView.iconSize.height)
                    .foregroundColor(.black)
            } //: Button
        )
    }
}
