//
//  Optional+Helpers.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import Foundation

extension Optional {
    var isNil: Bool {
        self == nil
    }
    
    var isNotNil: Bool {
        !isNil
    }
}
