//
//  MainRouter.swift
//  FootballCompetitions
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import SwiftUI

final class MainRouter: Router {
    static let shared: MainRouter = .init()
    
    private var window: UIWindow? {
        return SceneDelegate.shared.window
    }
    
    func openHome() {
        window?.rootViewController = HomeView().rootView
    }
    func openDetails(item: HomeItem) {
        navigationController?.presentView(DetailsView(viewModel: DetailsViewModel(item: item)))
    }
    
    func openSearchView(type: SearchItemType? = nil) {
        let viewModel = SearchViewModel(type: type)
        navigationController?.pushToView(SearchView(viewModel: viewModel))
    }
    
    func pop() {
        navigationController?.pop()
    }
}
