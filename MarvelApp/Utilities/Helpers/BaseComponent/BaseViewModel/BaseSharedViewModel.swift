//
//  BaseSharedViewModel.swift
//  FootballCompetitions
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import Foundation
import Combine

final class BaseSharedViewModel: DisposeObject, ObservableObject {
    // MARK: - DESTINATIONS
    enum Destination {
        case home
    }
    
    // MARK: - INSTANCE
    static let shared = BaseSharedViewModel()
    
    // MARK: - PROPERTIES
    
    // MARK: OUTPUT
    private(set) var alertSubject: PassthroughSubject<AlertItem, Never>
    
    private override init() {
        self.alertSubject = .init()        
        super.init()
    }
}
