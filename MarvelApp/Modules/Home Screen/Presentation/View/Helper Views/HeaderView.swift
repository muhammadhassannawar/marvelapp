//
//  HeaderView.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 06/01/2023.
//

import SwiftUI

struct HeaderView: View {
    // MARK: - PROPERTIES
    let title: String
    let subtitle: String?
    let titleFont: Font?
    let subtitleFont: Font?
    let titleColor: Color
    let subtitleColor: Color
    let alignment: HorizontalAlignment
    let spacing: CGFloat
    
    // MARK: - Init
    init(
        title: String,
        subtitle: String? = nil,
        titleFont: Font? = Font.Heading.title_1,
        titleColor: Color = Color.Core.text_01,
        subtitleFont: Font? = Font.Body.body_1,
        subtitleColor: Color = Color.Core.text_02,
        alignment: HorizontalAlignment = .leading,
        spacing: CGFloat = Dimensions.SectionHeaderView.spacing
    ) {
        self.title = title
        self.subtitle = subtitle
        self.titleFont = titleFont
        self.subtitleFont = subtitleFont
        self.titleColor = titleColor
        self.subtitleColor = subtitleColor
        self.alignment = alignment
        self.spacing = spacing
    }
    
    // MARK: - BODY
    var body: some View {
        VStack(alignment: alignment, spacing: spacing) {
            Text(title)
                .multilineTextAlignment(titleAlignment)
                .foregroundColor(titleColor)
                .font(titleFont)
                .fixedSize(horizontal: false, vertical: true)
            
            subtitleView
        } //: VStack
    }
    
    private var subtitleView: AnyView? {
        guard let subtitle = subtitle else { return nil }
        
        return Text(subtitle)
            .multilineTextAlignment(subtitleAlignment)
            .foregroundColor(subtitleColor)
            .font(subtitleFont)
            .fixedSize(horizontal: false, vertical: true)
            .eraseToAnyView()
    }
    
    private var titleAlignment: TextAlignment {
        switch alignment {
        case .center: return .center
        case .trailing: return .trailing
        default: return .leading
        }
    }
    
    private var subtitleAlignment: TextAlignment {
        switch alignment {
        case .center: return .center
        case .trailing: return .trailing
        default: return .leading
        }
    }
}
