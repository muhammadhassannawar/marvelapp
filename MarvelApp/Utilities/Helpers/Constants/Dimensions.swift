//
//  Dimensions.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 06/01/2023.
//

import SwiftUI

enum Dimensions {
    // MARK: - Status Bar
    enum StatusBar {
        static var height: CGFloat? {
            guard let delegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate else { return 0.0 }
            return delegate.window?.windowScene?.statusBarManager?.statusBarFrame.height
        }
    }
    
    // MARK: - HomeSectionView
    enum HomeSectionView {
        static let topPadding: CGFloat = 8.0
        static let horizontalPadding: CGFloat = 16.0
        static let bottomPadding: CGFloat = 32.0
    }
    
    // MARK: - RectangleItemView
    enum RectangleItemView {
        static let size: CGSize = CGSize(width: 200.0, height: 72.0)
        static let cornerRadius: CGFloat = 20.0
    }
    
    // MARK: - SectionHeaderView
    enum SectionHeaderView {
        static let spacing: CGFloat = 12.0
    }
    
    // MARK: - CircleItemView
    enum CircleItemView {
        static let size: CGSize = CGSize(width: 72.0, height: 72.0)
        static let padding: CGFloat = 4.0
    }
    
    // MARK: - ItemView
    enum ItemView {
        static let size: CGSize = CGSize(width: (UIScreen.screenWidth / 2) - 20.0, height: 296.0)
        static let cornerRadius: CGFloat = 25.0
    }
    
    // MARK: - Home
    enum Home {
        static let padding: CGFloat = 16.0
        static let headerImageHeight: CGFloat = 300.0
        static let bottomPadding: CGFloat = 20.0
        static let topPadding: CGFloat = 100.0
        static let imagesListTopPadding: CGFloat = 30.0
        static let imagesListHeight: CGFloat = 200.0
        static let imagesListCornerRadius: CGFloat = 16.0
        
        // MARK: - Navigation
        enum Navigation {
            static let navigationLogoSize: CGSize = CGSize(width: 60.0, height: 50.0)
        }
        
        // MARK: - CarouselCollectionView
        enum CarouselCollectionView {
            static let capsuleWidth: CGFloat = 7.0
            static let capsuleHeight: CGFloat = 7.0
            static let capsuleSpacing: CGFloat = 8.0
            static let selectedCapsuleWidth: CGFloat = 24.0
            static let contentHeight: CGFloat = 250.0
            static let contentSpacing: CGFloat = 12.0
            static let bottomPadding: CGFloat = 6.0
            static let contentWidth: CGFloat = UIScreen.screenWidth - 50.0
        }
        
        // MARK: - SquareItem
        enum SquareItem {
            static let imageSize: CGSize = CGSize(width: 184.0, height: 100.0)
            static let itemSize: CGSize = CGSize(width: 184.0, height: 168.0)
            static let textsPadding: CGFloat = 12.0
            static let cornerRadius: CGFloat = 16.0
            static let shadowRadius: CGFloat = 2.0
            static let itemPadding: CGFloat = 4.0
        }
    }
    
    // MARK: - SelectableItemView
    enum SelectableItemView {
        static let cornerRadius: CGFloat = 12.0
        static let frameHeight: CGFloat = 56.0
        static let stackSpacing: CGFloat = 8.0
        static let verticalStackSpacing: CGFloat = 2.0
        static let titleHeight: CGFloat = 16.0
        static let padding: CGFloat = 8.0
    }
    
    // MARK: - Button
    enum Button {
        static let padding: CGFloat = 16.0
    }
    
    // MARK: - NavigationView
    enum NavigationView {
        static let topPadding: CGFloat = 26.0
        static let spacing: CGFloat = 8.0
        static let horizontalPadding: CGFloat = 16.0
    }
    
    // MARK: - SearchView
    enum SearchView {
        static let horizontalPadding: CGFloat = 16.0
        static let spacing: CGFloat = 10.0
        static let contentPadding: CGFloat = 8.0
        
        // MARK: - ListViewItem
        enum ListViewItem {
            static let height: CGFloat = 280.0
            static let verticalPadding: CGFloat = 10.0
        }
    }
    
    // MARK: - SearchNavigationView
    enum SearchNavigationView {
        static let padding: CGFloat = 16.0
        static let navigationViewSize: CGSize = CGSize(width: 184.0, height: 184.0)
        static let contentSpacing: CGFloat = 28.0
        
        // MARK: - SearchView
        enum SearchView {
            static let cornerRadius: CGFloat = 12.0
            static let SearchViewHeight: CGFloat = 56.0
            static let offsetY: CGFloat = -28.0
            static let containerHeight: CGFloat = 14.0
            static let iconSize: CGSize = CGSize(width: 24.0, height: 24.0)
        }
    }
    
    // MARK: - DetailsView
    enum DetailsView {
        static let imageViewPadding: CGFloat = 40.0
        static let imageViewSize = CGSize(width: UIScreen.screenWidth * 0.5,
                                          height: UIScreen.screenWidth * 0.5)
    }
    
    // MARK: - BaseBottomSheetHeader
    enum BaseBottomSheetHeader {
        static let iconSize: CGSize = CGSize(width: 24.0, height: 24.0)
    }
    
    // MARK: - BaseBottomSheet
    enum BaseBottomSheet {
        static let insets: UIEdgeInsets = UIEdgeInsets(top: 24.0, left: 16.0, bottom: 16.0, right: 16.0)
    }
    
    // MARK: - ActivityIndicator
    enum ActivityIndicator {
        static let size: CGSize = CGSize(width: 50, height: 50)
    }
}
