//
//  HStackViewPositionModifier.swift
//  FootballCompetitions
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import SwiftUI

struct HStackViewPositionModifier: ViewModifier {
    // MARK: - Properties
    let position: ViewPosition
    
    // MARK: - Init
    init(position: ViewPosition) {
        self.position = position
    }
    
    // MARK: - BODY
    func body(content: Content) -> some View {
        switch position {
        case .center:
            return HStack {
                Spacer()
                content
                Spacer()
            } //: HStack
            .eraseToAnyView()
            
        case .leading:
            return HStack {
                content
                Spacer()
            } //: HStack
            .eraseToAnyView()
            
        case .trailing:
            return HStack {
                Spacer()
                content
            } //: HStack
            .eraseToAnyView()
        }
    }
    
    enum ViewPosition {
        case center
        case trailing
        case leading
    }
}
