//
//  HomeViewModel.swift
//  MoviesList
//
//  Created by Mohamed Hassan Nawar on 07/01/2023.
//

import Foundation
import Combine

final class HomeViewModel: HomeViewModelContract {
    // MARK: - PROPERTIES
    let title = Localizable.Home.title
    private(set) var images = {
        return ImageSlideshow.allCases
    }
    private let comicsUseCase: ComicsUseCaseContract
    private let eventsUseCase: EventsUseCaseContract
    private let charactersUseCase: CharactersUseCaseContract
    private let storiesUseCase: StoriesUseCaseContract
    private let seriesUseCase: SeriesUseCaseContract
    
    // MARK: - OUTPUT
    @Published private(set) var comics: [HomeItem] = []
    @Published private(set) var events: [HomeItem] = []
    @Published private(set) var characters: [HomeItem] = []
    @Published private(set) var stories: [HomeItem] = []
    @Published private(set) var series: [HomeItem] = []
    
    @Published private(set) var comicsState: ViewModelState = .idle
    @Published private(set) var eventsState: ViewModelState = .idle
    @Published private(set) var charactersState: ViewModelState = .idle
    @Published private(set) var storiesState: ViewModelState = .idle
    @Published private(set) var seriesState: ViewModelState = .idle
    
    @Published var currentIndex: Int = .zero
    
    // MARK: - INIT
    init(comicsUseCase: ComicsUseCaseContract = ComicsUseCase(),
         eventsUseCase: EventsUseCaseContract = EventsUseCase(),
         charactersUseCase: CharactersUseCaseContract = CharactersUseCase(),
         storiesUseCase: StoriesUseCaseContract = StoriesUseCase(),
         seriesUseCase: SeriesUseCaseContract = SeriesUseCase()
    ) {
        self.comicsUseCase = comicsUseCase
        self.eventsUseCase = eventsUseCase
        self.charactersUseCase = charactersUseCase
        self.storiesUseCase = storiesUseCase
        self.seriesUseCase = seriesUseCase
        super.init()
        
        self.viewDidLoad()
        listenToStates()
    }
    
    // MARK: - OUTPUT METHODS
    func viewDidLoad() {
        loadSeries()
        loadComics()
        loadStories()
        loadCharacters()
        loadEvents()
    }
}

// MARK: - PRIVATE METHODS
private extension HomeViewModel {
    func listenToStates() {
        Publishers
            .Merge5($comicsState, $charactersState,
                    $eventsState, $storiesState, $seriesState)
            .receive(on: RunLoop.main)
            .sink {
                self.state = $0
                self.alertItem = self.state.alertItem
            }
            .store(in: &cancellables)
    }
    
    func loadComics() {
        comicsState = .loading
        comicsUseCase.execute(searchKey: .empty, offset: .zero) { [weak self] (result, errorMessage) in
            guard let self = self else { return }
            if let result = result {
                self.comicsState = .successful
                self.comics = result.data.results
            } else if let error = errorMessage {
                self.comicsState = .failed(message: error)
            }
        }
    }
    
    func loadSeries() {
        seriesState = .loading
        seriesUseCase.execute(searchKey: .empty, offset: .zero) { [weak self] (result, errorMessage) in
            guard let self = self else { return }
            if let result = result {
                self.seriesState = .successful
                self.series = result.data.results
            } else if let error = errorMessage {
                self.seriesState = .failed(message: error)
            }
        }
    }
    
    func loadEvents() {
        eventsState = .loading
        eventsUseCase.execute(searchKey: .empty, offset: .zero) { [weak self] (result, errorMessage) in
            guard let self = self else { return }
            if let result = result {
                self.eventsState = .successful
                self.events = result.data.results
            } else if let error = errorMessage {
                self.eventsState = .failed(message: error)
            }
        }
    }
    
    func loadStories() {
        storiesState = .loading
        storiesUseCase.execute(searchKey: .empty, offset: .zero) { [weak self] (result, errorMessage) in
            guard let self = self else { return }
            if let result = result {
                self.storiesState = .successful
                self.stories = result.data.results
            } else if let error = errorMessage {
                self.storiesState = .failed(message: error)
            }
        }
    }
    
    func loadCharacters() {
        charactersState = .loading
        charactersUseCase.execute(searchKey: .empty, offset: .zero) { [weak self] (result, errorMessage) in
            guard let self = self else { return }
            if let result = result {
                self.charactersState = .successful
                self.characters = result.data.results
            } else if let error = errorMessage {
                self.charactersState = .failed(message: error)
            }
        }
    }
}
