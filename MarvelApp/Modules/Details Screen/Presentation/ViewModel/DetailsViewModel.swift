//
//  DetailsViewModel.swift
//  MoviesList
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import Foundation

final class DetailsViewModel: DetailsViewModelContract {
    // MARK: - PROPERTIES
    private var item: HomeItem
    
    // MARK: - OUTPUT
    var image: String {
        return item.itemImageName.value
    }
    var itemTitle: String {
        return item.itemTitle.value
    }
    var itemSubTitle: String {
        return item.itemSubTitle.value
    }
    
    // MARK: - INIT
    init(item: HomeItem) {
        self.item = item
        super.init()
    }
}
