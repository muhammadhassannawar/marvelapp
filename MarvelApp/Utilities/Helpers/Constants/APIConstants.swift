//
//  APIConstants.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 06/01/2023.
//

import Foundation

public enum APIConstants: String {
    case baseUrl = "https://gateway.marvel.com/v1/public"
    case hash = "hash"
    case apiKey = "apikey"
    case tsKey = "ts"
    case apiKeyValue = "7d94e84bcd551a305b50d27409313edf"
    case hashKeyValue = "b50f65495419aca4c025403d7ef70a0e"
    case comics = "/comics"
    case characters = "/characters"
    case stories = "/stories"
    case events = "/events"
    case series = "/series"
}
