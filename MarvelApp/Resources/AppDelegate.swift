//
//  AppDelegate.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import UIKit

@main
final class AppDelegate: UIResponder, UIApplicationDelegate {
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        return true
    }
}
