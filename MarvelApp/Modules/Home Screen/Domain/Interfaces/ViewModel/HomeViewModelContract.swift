//
//  HomeViewModelContract.swift
//  MoviesList
//
//  Created by Mohamed Hassan Nawar on 07/01/2023.
//

import Foundation

typealias HomeViewModelContract = BaseViewModel & ObservableObject & HomeViewModelInput & HomeViewModelOutput

protocol HomeViewModelInput {}

protocol HomeViewModelOutput {
    var comicsState: ViewModelState { get }
    var eventsState: ViewModelState { get }
    var charactersState: ViewModelState { get }
    var storiesState: ViewModelState { get }
    var seriesState: ViewModelState { get }
    var comics: [HomeItem] { get }
    var events: [HomeItem] { get }
    var characters: [HomeItem] { get }
    var stories: [HomeItem] { get }
    var series: [HomeItem] { get }
    func viewDidLoad()
}
