//
//  ComicsUseCase.swift
//  MoviesList
//
//  Created by Mohamed Hassan Nawar on 07/01/2023.
//

import Combine

final class ComicsUseCase: ComicsUseCaseContract {
    // MARK: - PROPERTIES
    let repo: HomeRepositoryContract
    
    // MARK: - INIT
    init(repo: HomeRepositoryContract = HomeRepository()) {
        self.repo = repo
    }
    
    // MARK: - METHODS
    func execute(searchKey: String? = nil, offset: Int? = nil,
                 completionBlock: @escaping (HomeResponse?, String?) -> Void) {
        var parameter = [Localizable.API.offset: offset.value] as [String: AnyObject]
        if searchKey.value.isNotEmpty {
            parameter.updateValue(searchKey.value as AnyObject, forKey: Localizable.API.nameStartsWith)
        }
        return repo.loadComics(using: parameter, completionBlock: completionBlock)
    }
}
