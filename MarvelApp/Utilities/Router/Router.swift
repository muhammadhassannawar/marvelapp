//
//  Router.swift
//  FootballCompetitions
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import UIKit
import SwiftUI

enum PresentationStyle {
    case push
    case present
}

protocol Router {
    var rootNavigationController: UINavigationController? { get }
    var navigationController: UINavigationController? { get }
    func dismiss(animated: Bool, completion: (() -> Void)?)
    func pop(animated: Bool) 
}

extension Router {
    var rootNavigationController: UINavigationController? {
        SceneDelegate.shared.window?.rootViewController as? UINavigationController
    }
    
    func pop(animated: Bool = true) {
        navigationController?.pop(animated: animated)
    }
    
    var navigationController: UINavigationController? {
        UIApplication.shared.topController as? UINavigationController
    }
    
    func close(using style: PresentationStyle, animated: Bool = true) {
        style == .push ? pop(animated: animated) : dismiss(animated: animated)
    }
    
    func dismiss(animated: Bool = true, completion: (() -> Void)? = nil) {
        navigationController?.dismissView(animated: animated, completion: completion)
    }
}
