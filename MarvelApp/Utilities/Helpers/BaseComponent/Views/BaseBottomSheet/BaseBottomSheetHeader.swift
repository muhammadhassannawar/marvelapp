//
//  BaseBottomSheetHeader.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 06/01/2023.
//

import SwiftUI

struct BaseBottomSheetHeader: View {
    // MARK: - PROPERTIES
    let title: String
    let titleFont: Font
    let titleColor: Color
    
    let action: () -> Void
    
    // MARK: - BODY
    var body: some View {
        HStack {
            titleView
            Spacer()
            actionView
        } //: HStack
    }
    
    private var titleView: some View {
        Text(title)
            .font(titleFont)
            .foregroundColor(titleColor)
    }
    
    private var actionView: some View {
        Button(
            action: action,
            label: {
                Image
                    .Arrows
                    .cross_close
                    .resizable()
                    .renderingMode(.template)
                    .frame(width: Dimensions.BaseBottomSheetHeader.iconSize.width,
                           height: Dimensions.BaseBottomSheetHeader.iconSize.height,
                           alignment: .center)
                    .foregroundColor(Color.Core.text_01)
            }
        )
    }
}
