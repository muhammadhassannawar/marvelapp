//
//  SearchItemTypes.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 15/01/2023.
//

import Foundation

// MARK: - SearchItemTypes
struct SearchItemTypes {
    var selectedType: SearchItemType?
}

enum SearchItemType: String, CaseIterable {
    case series, characters, events, comics, stories
}

extension SearchItemType: Identifiable {
    var id: Self { self }
}
