//
//  UINavigationController+Helpers.swift
//  FootballCompetitions
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import SwiftUI

typealias NavigationEnvironment = _EnvironmentKeyWritingModifier<UINavigationController>

// MARK: - GENERAL
extension UINavigationController {
    func pop(animated: Bool = true) {
        popViewController(animated: animated)
    }
    
    func dismissView(animated: Bool = true, completion: (() -> Void)? = nil) {
        dismiss(animated: animated, completion: completion)
    }
    
    func pushToView<Content: View>(_ view: Content, animated: Bool = true) {
        pushViewController(
            view.viewController(using: .push, and: self),
            animated: animated
        )
    }
}

// MARK: - PRESENT
extension UINavigationController {
    func presentView<Content: View>(
        _ view: Content,
        animated: Bool = true,
        withNavigation: Bool = true,
        modalStyle: UIModalPresentationStyle = .fullScreen,
        transitionStyle: UIModalTransitionStyle = .coverVertical,
        completion: (() -> Void)? = nil
    ) {
        guard withNavigation else {
            let viewController = view.viewController(using: .present, and: self)
            viewController.modalPresentationStyle = modalStyle
            viewController.modalTransitionStyle = transitionStyle
            
            present(
                viewController,
                animated: animated,
                completion: completion
            )
            return
        }
        
        let navigationController = UINavigationController()
        navigationController.setViewControllers([
            view.viewController(using: .present, and: navigationController)
        ], animated: true)
        navigationController.modalPresentationStyle = modalStyle
        navigationController.modalTransitionStyle = transitionStyle
        
        present(
            navigationController,
            animated: animated,
            completion: completion
        )
    }
}
