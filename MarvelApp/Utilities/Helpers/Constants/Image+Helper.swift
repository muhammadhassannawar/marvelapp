//
//  Image+Helper.swift
//  FootballCompetitions
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import SwiftUI

extension Image {
    // MARK: - Home
    enum Home {
        static let magnifyingGlass = "magnifyingglass".systemImage
        static let marvelLogo = "logo_marvel".image
    }
    
    // MARK: - Search
    enum Search {
        static let remove = "xmark.circle.fill".systemImage
    }
    
    // MARK: - Arrows
    enum Arrows {
        static let cross_close = "cross_close".image
        static let arrow_left = "arrow_left".image
        static let chevron_right = "chevron_right".image
    }
}

extension String {
    var image: Image {
        return Image(self)
    }
    
    var systemImage: Image {
        return Image(systemName: self)
    }
}
