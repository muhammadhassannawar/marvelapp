//
//  HomeRepository.swift
//  MoviesList
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import Combine

final class HomeRepository: DisposeObject, HomeRepositoryContract {
    // MARK: - PROPERTIES
    private let service: HomeServiceContract
    
    // MARK: - INIT
    init(service: HomeServiceContract = HomeService()) {
        self.service = service
        
        super.init()
    }
    
    // MARK: - REPOSITORIES METHODS
    func loadCharacters(using parameters: [String: AnyObject],
                        completionBlock: @escaping (HomeResponse?, String?) -> Void) {
        return service.loadCharacters(using: parameters, completionBlock: completionBlock)
    }
    
    func loadComics(using parameters: [String: AnyObject],
                    completionBlock: @escaping (HomeResponse?, String?) -> Void) {
        return service.loadComics(using: parameters, completionBlock: completionBlock)
    }
    
    func loadEvents(using parameters: [String: AnyObject],
                    completionBlock: @escaping (HomeResponse?, String?) -> Void) {
        return service.loadEvents(using: parameters, completionBlock: completionBlock)
    }
    
    func loadSeries(using parameters: [String: AnyObject],
                    completionBlock: @escaping (HomeResponse?, String?) -> Void) {
        return service.loadSeries(using: parameters, completionBlock: completionBlock)
    }
    
    func loadStories(using parameters: [String: AnyObject],
                     completionBlock: @escaping (HomeResponse?, String?) -> Void) {
        return service.loadStories(using: parameters, completionBlock: completionBlock)
    }
}
