//
//  HomeService.swift
//  MoviesList
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import Combine

final class HomeService: DisposeObject, HomeServiceContract {
    // MARK: - Properties
    var apiService: CoreNetworkProtocol
    
    // MARK: - INIT
    init(apiService: CoreNetworkProtocol = CoreNetwork()) {
        self.apiService = apiService
        super.init()
    }
    
    // MARK: - SERVICE METHODS
    func loadSeries(using parameters: [String: AnyObject], completionBlock: @escaping (HomeResponse?, String?) -> Void) {
        loadHomeRequest(using: parameters, with: .series,
                        completionBlock: completionBlock)
    }
    
    func loadEvents(using parameters: [String: AnyObject], completionBlock: @escaping (HomeResponse?, String?) -> Void) {
        loadHomeRequest(using: parameters, with: .events,
                        completionBlock: completionBlock)
    }
    
    func loadStories(using parameters: [String: AnyObject], completionBlock: @escaping (HomeResponse?, String?) -> Void) {
        loadHomeRequest(using: parameters, with: .stories,
                        completionBlock: completionBlock)
    }
    
    func loadCharacters(using parameters: [String: AnyObject], completionBlock: @escaping (HomeResponse?, String?) -> Void) {
        loadHomeRequest(using: parameters, with: .characters,
                        completionBlock: completionBlock)
    }
    
    func loadComics(using parameters: [String: AnyObject], completionBlock: @escaping (HomeResponse?, String?) -> Void) {
        loadHomeRequest(using: parameters, with: .comics,
                        completionBlock: completionBlock)
    }
}

private extension HomeService {
    func loadHomeRequest(using parameters: [String: AnyObject], with endPoint: APIConstants, completionBlock: @escaping (HomeResponse?, String?) -> Void) {
        let request = RequestSpecs<HomeResponse>(method: .GET, urlString: endPoint.rawValue, parameters: parameters)
        apiService.makeRequest(request: request) { (result, error) in
            completionBlock(result, error?.localizedDescription)
        }
    }
}
