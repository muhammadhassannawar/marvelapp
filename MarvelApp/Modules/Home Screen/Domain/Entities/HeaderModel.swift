//
//  HeaderModel.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 15/01/2023.
//

import Foundation

// MARK: - HeaderModel
struct HeaderModel {
    let title: String
    let description: String? = nil
    let buttonData: ButtonAction?
}

// MARK: - ButtonAction
struct ButtonAction {
    var title: String
    var action: () -> Void
}
