//
//  HostService.swift
//  FootballCompetitions
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import UIKit

struct HostService {
    static func getBaseURL() -> String {
        return APIConstants.baseUrl.rawValue
    }
    static var headers: [String: String] { [:] }
    
    static var extraParameters: [String: AnyObject] {
        [APIConstants.hash.rawValue: APIConstants.hashKeyValue.rawValue,
         APIConstants.apiKey.rawValue: APIConstants.apiKeyValue.rawValue,
         APIConstants.tsKey.rawValue: 0
        ] as [String: AnyObject]
    }
}
