//
//  SearchViewModel.swift
//  MoviesList
//
//  Created by Mohamed Hassan Nawar on 01/06/2022.
//

import Combine
import Foundation

final class SearchViewModel: SearchViewModelContract {
    // MARK: - PROPERTIES
    private let comicsUseCase: ComicsUseCaseContract
    private let eventsUseCase: EventsUseCaseContract
    private let charactersUseCase: CharactersUseCaseContract
    private let storiesUseCase: StoriesUseCaseContract
    private let seriesUseCase: SeriesUseCaseContract
    private var areThereMoreItems = true
    private var startFrom: Int = .zero
    
    // MARK: - OUTPUT
    @Published private(set) var searchItems: [HomeItem] = []
    @Published private(set) var searchState: ViewModelState = .idle
    private(set) var scrollToItemSubject: PassthroughSubject<Int, Never> = .init()
    @Published var shouldShowTypesSheet: Bool = false
    
    @Published var searchText: String = .empty {
        didSet{
            if searchText.isEmpty { resetSearch() }
        }
    }
    
    @Published var type: SearchItemTypes? = .init() {
        didSet {
            resetSearch()
        }
    }
    
    var navigationTitle: String {
        Localizable.General.search
    }
    
    var searchPlaceholder: String {
        Localizable.General.search
    }
    
    // MARK: - INIT
    init(comicsUseCase: ComicsUseCaseContract = ComicsUseCase(),
         eventsUseCase: EventsUseCaseContract = EventsUseCase(),
         charactersUseCase: CharactersUseCaseContract = CharactersUseCase(),
         storiesUseCase: StoriesUseCaseContract = StoriesUseCase(),
         seriesUseCase: SeriesUseCaseContract = SeriesUseCase(),
         type: SearchItemType? = nil
    ) {
        self.comicsUseCase = comicsUseCase
        self.eventsUseCase = eventsUseCase
        self.charactersUseCase = charactersUseCase
        self.storiesUseCase = storiesUseCase
        self.seriesUseCase = seriesUseCase
        
        super.init()
        self.type?.selectedType = type
    }
    
    // MARK: - INPUT METHODS
    func resetState() {
        searchState = .idle
    }
    
    func didPressSearch() {
        guard searchText.isNotEmpty else {return}
        guard (type?.selectedType).isNotNil else {
            self.alertItem =  AlertItem(message: Localizable.SearchView.selectSearchType)
            return
        }
        resetSearch()
    }
    
    func didSelectType(using type: SearchItemType?, offset: Int) {
        guard let selectedType = type else {
            return
        }
        
        switch selectedType {
        case .series:
            loadSeries(using: searchText, using: offset)
        case .characters:
            loadCharacters(using: searchText, using: offset)
        case .events:
            loadEvents(using: searchText, using: offset)
        case .comics:
            loadComics(using: searchText, using: offset)
        case .stories:
            loadStories(using: searchText, using: offset)
        }
    }
    
    func loadNextPage() {
        guard areThereMoreItems else { return }
        startFrom += Constants.receivedItemCount
        didSelectType(using: type?.selectedType, offset: startFrom)
    }
}

// MARK: - PRIVATE METHODS
private extension SearchViewModel {
    func resetSearch() {
        startFrom = .zero
        didSelectType(using: type?.selectedType, offset: startFrom)
    }
    func scrollToSelectedItem() {
        guard let selectedItemId = searchItems[searchItems.count - Constants.receivedItemCount].id  else { return }
        scrollToItemSubject.send(selectedItemId)
        
    }
    
    func loadComics(using searchKey: String, using offset: Int) {
        searchState = .loading
        comicsUseCase.execute(searchKey: searchKey,
                              offset: offset) { [weak self] (result, errorMessage) in
            guard let self = self else { return }
            if let result = result {
                self.searchState = .successful
                self.didLoadItems(models: result.data.results, startFrom: self.startFrom)
            } else if let error = errorMessage {
                self.searchState = .failed(message: error)
            }
        }
    }
    
    func loadSeries(using searchKey: String, using offset: Int) {
        searchState = .loading
        seriesUseCase.execute(searchKey: searchKey,
                              offset: offset) { [weak self] (result, errorMessage) in
            guard let self = self else { return }
            if let result = result {
                self.searchState = .successful
                self.didLoadItems(models: result.data.results, startFrom: self.startFrom)
            } else if let error = errorMessage {
                self.searchState = .failed(message: error)
            }
        }
    }
    
    func loadEvents(using searchKey: String, using offset: Int) {
        searchState = .loading
        eventsUseCase.execute(searchKey: searchKey,
                              offset: offset) { [weak self] (result, errorMessage) in
            guard let self = self else { return }
            if let result = result {
                self.searchState = .successful
                self.didLoadItems(models: result.data.results, startFrom: self.startFrom)
            } else if let error = errorMessage {
                self.searchState = .failed(message: error)
            }
        }
    }
    
    func loadStories(using searchKey: String, using offset: Int) {
        searchState = .loading
        storiesUseCase.execute(searchKey: searchKey,
                               offset: offset) { [weak self] (result, errorMessage) in
            guard let self = self else { return }
            if let result = result {
                self.searchState = .successful
                self.didLoadItems(models: result.data.results, startFrom: self.startFrom)
            } else if let error = errorMessage {
                self.searchState = .failed(message: error)
            }
        }
    }
    
    func loadCharacters(using searchKey: String, using offset: Int) {
        searchState = .loading
        charactersUseCase.execute(searchKey: searchKey,
                                  offset: offset) { [weak self] (result, errorMessage) in
            guard let self = self else { return }
            if let result = result {
                self.searchState = .successful
                self.didLoadItems(models: result.data.results, startFrom: self.startFrom)
            } else if let error = errorMessage {
                self.searchState = .failed(message: error)
            }
        }
    }
    
    func didLoadItems(models: [HomeItem], startFrom: Int) {
        if models.count < Constants.receivedItemCount {
            areThereMoreItems = false
        }else{
            if startFrom == .zero {
                self.searchItems = models
            } else {
                searchItems.append(contentsOf: models)
                scrollToSelectedItem()
            }
            areThereMoreItems = true
        }
    }
}
