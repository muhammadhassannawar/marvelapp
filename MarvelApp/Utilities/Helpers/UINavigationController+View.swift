//
//  UINavigationController+View.swift
//  FootballCompetitions
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import SwiftUI

// MARK: - VIEWS
extension View {
    var rootView: UINavigationController {
        let navigationController: UINavigationController = .init()
        let rootView = self.navigationController(navigationController)
        navigationController.setViewControllers([HostingController(rootView: rootView)], animated: true)
        
        return navigationController
    }
    
    func viewController(
        using presentationStyle: PresentationStyle,
        and navigationController: UINavigationController
    ) -> UIViewController {
        HostingController(
            rootView: self.presentationStyle(presentationStyle).navigationController(navigationController)
        )
    }
    
    func presentationStyle(_ value: PresentationStyle) -> some View {
        environment(\.presentationStyle, value)
    }
    
    var viewController: UIViewController {
        HostingController(
            rootView: self
        )
    }
    
    func navigationController(_ value: UINavigationController) -> some View {
        environment(\.navigationController, value)
    }
}

// MARK: - ENVIRONMENT
enum NavigationControllerKey: EnvironmentKey {
    static var defaultValue: UINavigationController {
        UINavigationController()
    }
}

extension EnvironmentValues {
    var navigationController: UINavigationController {
        get { self[NavigationControllerKey.self] }
        set { self[NavigationControllerKey.self] = newValue }
    }
}

enum PresentationStyleKey: EnvironmentKey {
    static var defaultValue: PresentationStyle = .push
}

extension EnvironmentValues {
    var presentationStyle: PresentationStyle {
        get { self[PresentationStyleKey.self] }
        set { self[PresentationStyleKey.self] = newValue }
    }
}
