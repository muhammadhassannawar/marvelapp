//
//  Gradient+Assets.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 06/01/2023.
//

import SwiftUI

extension Gradient {
    static var gradient_01: Gradient {
        return .init(colors: [Color.Gradient.gradient_01_start, Color.Gradient.gradient_01_end])
    }
    
    static var gradient_02: Gradient {
        return .init(colors: [Color.Gradient.gradient_02_start, Color.Gradient.gradient_02_end])
    }
}
