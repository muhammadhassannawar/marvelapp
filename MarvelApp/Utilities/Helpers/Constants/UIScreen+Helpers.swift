//
//  UIScreen+Helpers.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 06/01/2023.
//

import UIKit

extension UIScreen {
   static let screenWidth = UIScreen.main.bounds.size.width
   static let screenHeight = UIScreen.main.bounds.size.height
}
