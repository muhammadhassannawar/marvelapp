//
//  ListView.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 13/01/2023.
//

import SwiftUI
import Combine

struct ListView: View {
    // MARK: - PROPERTIES
    private var scrollToItemSubject: PassthroughSubject<Int, Never>
    private var items: [HomeItem]
    private var didReachLastItem: () -> Void
    private var didSelectItem: (HomeItem) -> Void
    
    // MARK: - INIT
    init(items: [HomeItem],
         scrollToItemSubject: PassthroughSubject<Int, Never>,
         didReachLastItem: @escaping () -> Void,
         didSelectItem: @escaping (HomeItem) -> Void) {
        self.scrollToItemSubject = scrollToItemSubject
        self.didSelectItem = didSelectItem
        self.didReachLastItem = didReachLastItem
        self.items = items
    }
    
    // MARK: - BODY
    var body: some View {
        ScrollViewReader { proxy in
            LazyVGrid(columns: [GridItem(.flexible()), GridItem(.flexible())],
                      alignment: .center) {
                ForEach(items, id: \.id) { item in
                    Button {
                        self.didSelectItem(item)
                    } label: {
                        ItemView(title: item.itemTitle, imageName: item.itemImageName, subtitle: item.itemSubTitle).eraseToAnyView()
                            .id(item.id)
                    } //: Button
                    .frame(height: Dimensions.SearchView.ListViewItem.height)
                    .padding(.vertical, Dimensions.SearchView.ListViewItem.verticalPadding)
                    .onAppear(perform: {
                        if items.isLastItem(item) {
                            self.didReachLastItem()
                        }
                    })
                } //: ForEach
                
            } //: LazyVGrid
                      .onReceive(scrollToItemSubject) { itemId in
                          proxy.scrollTo(itemId, anchor: .top)
                      } //: onReceive
        } //: ScrollViewReader
    }
}
