//
//  Constants.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import Foundation
import SwiftUI

public enum Constants {
    static let typesListSheetOpacity: CGFloat = 0.5
    static let shadowRadius: CGFloat = 6.0
    static let shadowX: CGFloat = 0.0
    static let shadowY: CGFloat = 1.0
    static let receivedItemCount: Int = 20
}

// MARK: - ImageSlideshow
enum HomeItemStyle: Int {
    case circle = 1
    case square = 2
    case verticalRectangle = 3
    case horizontalRectangle = 4
}

// MARK: - ImageSlideshow
enum ImageSlideshow: String, CaseIterable {
    case imageSlideshow1
    case imageSlideshow2
    case imageSlideshow3
    case imageSlideshow4
    case imageSlideshow5
    case imageSlideshow6
    case imageSlideshow7
    case imageSlideshow8
    case imageSlideshow9
    case imageSlideshow10
    
    var image: Image {
        return self.rawValue.image
    }
}
