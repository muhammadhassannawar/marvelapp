//
//  RoundedCorner.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 06/01/2023.
//

import SwiftUI

struct RoundedCorner: Shape {
    // MARK: - PROPERTIES
    let radius: CGFloat
    let corners: UIRectCorner
    
    // MARK: - PATH
    func path(in rect: CGRect) -> Path {
        return Path(
            UIBezierPath(
                roundedRect: rect,
                byRoundingCorners: corners,
                cornerRadii: CGSize(width: radius, height: radius)
            ).cgPath
        )
    }
}
