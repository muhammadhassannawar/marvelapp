//
//  Int+Helpers.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 06/01/2023.
//

import Foundation

public extension Optional where Wrapped == Int {
    var value: Int {
        return self ?? 0
    }
}

extension Int {
    var toString: String {
        return "\(self)"
    }
}
