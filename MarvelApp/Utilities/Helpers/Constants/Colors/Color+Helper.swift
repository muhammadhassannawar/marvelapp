//
//  Color+Helper.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import SwiftUI

extension Color {
    enum Core {
        static let text_01 = "text_01".color
        static let text_02 = "text_02".color
        static let text_03 = "text_03".color
        static let text_04 = "text_04".color
    }
    
    enum Gradient {
        static let gradient_01_start = "gradient_01_start".color
        static let gradient_01_end = "gradient_01_end".color
        static let gradient_02_start = "gradient_02_start".color
        static let gradient_02_end = "gradient_02_end".color
    }
    
    static let shadow_color = "shodow_color".color
}

private extension String {
    var color: Color {
        return Color(self)
    }
    
    var uiColor: UIColor {
        return UIColor(named: self) ?? .clear
    }
}

