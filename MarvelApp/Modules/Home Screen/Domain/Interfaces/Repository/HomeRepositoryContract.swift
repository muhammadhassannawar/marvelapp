//
//  HomeRepositoryContract.swift
//  MoviesList
//
//  Created by Mohamed Hassan Nawar on 07/01/2023.
//

import Combine

protocol HomeRepositoryContract {
    func loadCharacters(using parameters: [String: AnyObject],
                        completionBlock: @escaping (HomeResponse?, String?) -> Void)
    func loadComics(using parameters: [String: AnyObject],
                    completionBlock: @escaping (HomeResponse?, String?) -> Void)
    func loadEvents(using parameters: [String: AnyObject],
                    completionBlock: @escaping (HomeResponse?, String?) -> Void)
    func loadSeries(using parameters: [String: AnyObject],
                    completionBlock: @escaping (HomeResponse?, String?) -> Void)
    func loadStories(using parameters: [String: AnyObject],
                     completionBlock: @escaping (HomeResponse?, String?) -> Void)
}
