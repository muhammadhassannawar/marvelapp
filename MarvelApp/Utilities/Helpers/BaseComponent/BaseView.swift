//
//  BaseView.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 06/01/2023.
//

import SwiftUI

struct BaseView<Content>: View where Content: View {
    // MARK: - ROUTER
    private let router: MainRouter = .shared
    
    // MARK: - ENVIRONMENTS
    @Environment(\.localStatusBarStyle) var statusBarStyle
    
    // MARK: - VIEWMODEL
    @StateObject var viewModel: BaseSharedViewModel = .shared
    
    // MARK: - PROPERTIES
    let content: Content
    @Binding private var state: ViewModelState
    @Binding private var alertItem: AlertItem?
    
    // MARK: - INIT
    init(
        state: Binding<ViewModelState>,
        alertItem: Binding<AlertItem?>? = nil,
        @ViewBuilder content: () -> Content
    ) {
        self._state = state
        self._alertItem = alertItem ?? .constant(nil)
        self.content = content()
    }
    
    // MARK: - BODY
    var body: some View {
        ZStack {
            if state == .loading {
                activityIndicator
            } else {
                content
            }
        } //: ZStack
        .hideNavigationBar()
        .onAppear {
            statusBarStyle.currentStyle = .default
        } //: onAppear
        .onChange(of: alertItem, perform: didSetAlert(_:))
        .onReceive(viewModel.alertSubject, perform: didSetAlert(_:))
    }
    
    private func handleNavigation(_ destination: BaseSharedViewModel.Destination) {
        switch destination {
        case .home: goToHome()
        }
    }
    
    private var activityIndicator: some View {
        ActivityIndicator()
            .frame(width: Dimensions.ActivityIndicator.size.width,
                   height: Dimensions.ActivityIndicator.size.height)
            .foregroundColor(Color.Gradient.gradient_01_end)
    }
}

private extension BaseView {
    func goToHome() {
        router.openHome()
    }
}

// MARK: - UI HANDLERS
private extension BaseView {
    func didSetAlert(_ alert: AlertItem?) {
        guard let alert = alert else { return }
        UIApplication.shared.showAlertView(alert, completion: {
            alertItem = nil
        })
    }
}
