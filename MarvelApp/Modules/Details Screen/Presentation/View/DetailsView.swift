//
//  DetailsView.swift
//  MoviesList
//
//  Created by Mohamed Hassan Nawar on 31/05/2022.
//

import SwiftUI
import SDWebImageSwiftUI

struct DetailsView: View {
    // MARK: - ROUTER
    private let router: MainRouter = .shared
    
    // MARK: - PROPERTIES
    @StateObject var viewModel: DetailsViewModel
    
    // MARK: - BODY
    var body: some View {
        BaseView(
            state: $viewModel.state
        ) {
            ZStack(alignment: .top) {
                backgroundView
                    .edgesIgnoringSafeArea(.all)
                
                VStack(spacing: .zero) {
                    navigationView
                        .padding(.top, Dimensions.NavigationView.topPadding)
                    
                    BaseBottomSheet {
                        contentView
                            .scrollIfNeeded()
                    } //: BaseBottomSheet
                } //: VStack
            } //: ZStack
        } //: BaseView
    }
    
    private var navigationView: AnyView {
        Button {
            router.dismiss()
        } label: {
            Image.Arrows.cross_close
                .renderingMode(.template)
                .foregroundColor(Color.black)
        } //: Button
        .wrapInHStack(.leading)
        .padding(.horizontal, Dimensions.Button.padding)
        .eraseToAnyView()
    }
    
    private var backgroundView: some View {
        LinearGradient.gradient_02
    }
    
    private var contentView: AnyView {
        VStack {
            WebImage(url: URL(string: viewModel.image))
                .resizable()
                .frame(width: Dimensions.DetailsView.imageViewSize.width,
                       height: Dimensions.DetailsView.imageViewSize.height)
                .scaledToFill()
                .eraseToAnyView()
                .padding(Dimensions.DetailsView.imageViewPadding)
                .wrapInHStack(.center)
            
            HeaderView(
                title: viewModel.itemTitle,
                subtitle: viewModel.itemSubTitle,
                alignment: .center)
            
            Spacer()
        } //: VStack
        .eraseToAnyView()
    }
}
