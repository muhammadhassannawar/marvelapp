//
//  DisposeObject.swift
//  FootballCompetitions
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import Combine

class DisposeObject {
    var deinitCalled: (() -> Void)?
    var cancellables: Set<AnyCancellable>
    
    init() {
        self.cancellables = Set<AnyCancellable>()
    }
    
    deinit {
        self.cancellables.removeAll()
        deinitCalled?()
    }
}
