//
//  DetailsViewModelContract.swift
//  MoviesList
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import Combine

typealias DetailsViewModelContract = BaseViewModel & ObservableObject & DetailsViewModelOutput

protocol DetailsViewModelOutput {
    var image: String { get }
    var itemTitle: String { get }
    var itemSubTitle: String { get }
}
