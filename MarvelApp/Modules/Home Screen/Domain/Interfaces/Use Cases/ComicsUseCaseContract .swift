//
//  ComicsUseCaseContract .swift
//  MoviesList
//
//  Created by Mohamed Hassan Nawar on 07/01/2023.
//

import Combine

protocol ComicsUseCaseContract {
    func execute(searchKey: String?, offset: Int?,
                 completionBlock: @escaping (HomeResponse?, String?) -> Void)
}
