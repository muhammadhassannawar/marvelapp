//
//  CoreNetwork.swift
//  FootballCompetitions
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import UIKit

final class CoreNetwork {
    // MARK: - PROPERTIES
    static var sharedInstance: CoreNetworkProtocol = CoreNetwork()
    fileprivate lazy var networkCommunication: NetworkingInterface! = {
        return DefaultNetworkAdapter.init(baseURL: HostService.getBaseURL(),headers: HostService.headers, extraParameters: HostService.extraParameters)
    }()
}

extension CoreNetwork: CoreNetworkProtocol {
    func makeRequest<T: Codable>(request: RequestSpecs<T>,
                                 completion: @escaping (T?, Error?) -> Void) {
        networkCommunication.request(request, completionBlock: completion)
    }
}

protocol CoreNetworkProtocol {
    func makeRequest<T: Codable>(request: RequestSpecs<T>,
                                 completion: @escaping (T?, Error?) -> Void)
}
