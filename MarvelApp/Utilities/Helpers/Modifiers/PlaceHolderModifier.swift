//
//  PlaceHolderModifier.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 06/01/2023.
//

import SwiftUI

struct PlaceHolderModifier<T: View>: ViewModifier {
    // MARK: - PROPERTIES
    var placeHolder: T
    var show: Bool
    
    // MARK: - INIT
    init(_ placeholder: T, show: Bool) {
        self.placeHolder = placeholder
        self.show = show
    }
    
    // MARK: - BODY
    func body(content: Content) -> some View {
        ZStack(alignment: .leading) {
            if show { placeHolder }
            content
        }
    }
}

extension View {
    func placeholder<T: View>(_ holder: T, show: Bool) -> some View {
        self.modifier(PlaceHolderModifier(holder, show: show))
    }
}
