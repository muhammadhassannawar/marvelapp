# Overview

This repo includes a sample project that demonstrates the MVVM with clean architecture patterns on iOS.

* Xcode 14
* SwiftUI
* Combine

Show a list of comics, events, characters, stories and series of marvel world and allow search for specific item using search keyword and select a type.

API to use: https://gateway.marvel.com/v1/public


<img src="https://i.postimg.cc/y88tPWnK/Simulator-Screen-Shot-i-Phone-14-Pro-2023-01-16-at-01-04-23.png" width="160">  <img src="https://i.postimg.cc/3NhbDwWP/Simulator-Screen-Shot-i-Phone-14-Pro-2023-01-16-at-01-04-17.png" width="160">  <img src="https://i.postimg.cc/vHzCf5rX/Simulator-Screen-Shot-i-Phone-14-Pro-2023-01-16-at-01-03-58.png" width="160"> <img src="https://i.postimg.cc/cJKzpjV5/Simulator-Screen-Shot-i-Phone-14-Pro-2023-01-16-at-01-04-07.png" width="160">

# Pods
In this project includes some of useful libraries, such as:
* SDWebImageSwiftUI
