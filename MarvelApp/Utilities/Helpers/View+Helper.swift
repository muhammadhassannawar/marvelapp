//
//  View+Helper.swift
//  FootballCompetitions
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import SwiftUI

extension View {
    func hideNavigationBar() -> some View {
        self
            .navigationBarTitle(String.empty)
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
    }
}

extension View {
    func eraseToAnyView() -> AnyView {
        return AnyView(self)
    }
}

extension View {
    func wrapInHStack(_ position: HStackViewPositionModifier.ViewPosition) -> some View {
        modifier(HStackViewPositionModifier(position: position))
    }
}

extension View {
    func isHidden(_ isHidden: Bool) -> Self? {
        isHidden ? nil : self
    }
}

extension View {
    func addTextIfNeeded(word: String?) -> Text? {
        guard let word = word else { return nil }
        return Text(word)
    }
}

extension View {
    @ViewBuilder
    func overlay<Content: View>(when condition: Bool, _ content: Content) -> some View {
        if condition {
            overlay(content)
        } else {
            self
        }
    }
    
    @ViewBuilder
    func overlay(
        when condition: Bool,
        using color: Color
    ) -> some View {
        if condition {
            ZStack(alignment: .bottom) {
                color
                    .edgesIgnoringSafeArea(.top)
                
                self
            }
        } else {
            self
        }
    }
}

private extension Shape {
    func setBorder(
        color: Color,
        width: CGFloat
    ) -> some View {
        return stroke(color, lineWidth: width).eraseToAnyView()
    }
}
