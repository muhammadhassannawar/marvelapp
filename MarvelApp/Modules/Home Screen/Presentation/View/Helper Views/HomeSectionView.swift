//
//  HomeSectionView.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 06/01/2023.
//

import SwiftUI

struct HomeSectionView<Model: Identifiable>: View
where Model: HomeItemRepresentable {
    // MARK: - Properties
    let items: [Model]
    var style: HomeItemStyle = .circle
    let headerData: HeaderModel
    let onSelect: (Model) -> Void
    
    // MARK: - Body
    var body: some View {
        VStack {
            if items.count > .zero {
                SectionHeaderView(
                    title: headerData.title,
                    description: headerData.description,
                    buttonData: headerData.buttonData)
                .padding(.horizontal)
            }
            ScrollViewReader { value in
                ScrollView(.horizontal, showsIndicators: false) {
                    HStack {
                        ForEach(items) { item in
                            Button {
                                onSelect(item)
                            } label: {
                                itemView(with: item)
                            } //: Button
                        } //: ForEach
                        .padding(.top, Dimensions.HomeSectionView.topPadding)
                        .padding(.bottom, Dimensions.HomeSectionView.bottomPadding)
                        Color.clear
                    } //: HStack
                    .padding(.horizontal, Dimensions.HomeSectionView.horizontalPadding)
                } //: ScrollView
            } //: ScrollViewReader
        } //: VStack
    }
}

// MARK: - Private Methods
private extension HomeSectionView {
    func itemView(with item: HomeItemRepresentable) -> some View {
        switch style {
        case .circle:
            return CircleItemView(title: item.itemID?.toString).eraseToAnyView()
            
        case .horizontalRectangle:
            return RectangleItemView(title: item.itemTitle, imageName: item.itemImageName, subtitle: item.itemSubTitle).eraseToAnyView()
            
        case .square:
            return SquareItemView(title: item.itemTitle, imageName: item.itemImageName, subtitle: item.itemSubTitle).eraseToAnyView()
            
        case .verticalRectangle:
            return ItemView(title: item.itemTitle, imageName: item.itemImageName, subtitle: item.itemSubTitle).eraseToAnyView()
        }
    }
}
