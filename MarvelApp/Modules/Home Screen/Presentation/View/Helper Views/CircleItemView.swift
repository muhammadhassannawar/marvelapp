//
//  CircleItemView.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 06/01/2023.
//

import SwiftUI
import SDWebImageSwiftUI

protocol Initializable {
    init(title: String?, imageName: String?, subtitle: String?)
}

// MARK: - CircleItemView
struct CircleItemView: View, Initializable {
    // MARK: - Properties
    private let title: String?
    
    // MARK: - Init
    init(title: String?, imageName: String? = nil, subtitle: String? = nil) {
        self.title = title
    }
    
    // MARK: - Body
    var body: some View {
        VStack {
            Text(Localizable.Home.Stories.story + .space + title.value)
                .foregroundColor(Color.blue)
                .frame(width: Dimensions.CircleItemView.size.width, height: Dimensions.CircleItemView.size.height)
                .clipShape(Circle())
                .padding(Dimensions.CircleItemView.padding)
                .bordered()
        } //: VStack
    }
}
