//
//  SearchView+Content.swift
//  MoviesList
//
//  Created by Mohamed Hassan Nawar on 01/06/2022.
//

import SwiftUI

extension SearchView {
    var navigationView: AnyView {
        SearchNavigationView(
            title: viewModel.navigationTitle,
            searchText: $viewModel.searchText,
            onCommit: viewModel.didPressSearch,
            onEditing: viewModel.resetState
        )
        .eraseToAnyView()
    }
    
    var contentView: some View {
        VStack {
            SelectTypeView
                .padding(.horizontal, Dimensions.SearchView.horizontalPadding)
            
            ScrollView(showsIndicators: false) {
                ListView(items: viewModel.searchItems, scrollToItemSubject: viewModel.scrollToItemSubject, didReachLastItem: {
                    viewModel.loadNextPage()
                }, didSelectItem: { item in
                    router.openDetails(item: item)
                })
                .padding(Dimensions.SearchView.contentPadding)
            } //: ScrollView
            .frame(maxWidth: .infinity, alignment: .center)
        } //: VStack
    }
    
    var typesListSheet: some View {
        BaseBottomSheet(opacity: Constants.typesListSheetOpacity, title: Localizable.SearchView.searchTypes) {
            viewModel.shouldShowTypesSheet.toggle()
        } content: {
            typesList
        }
    }
    
    private var SelectTypeView: some View {
        SelectableItemView(
            title: Localizable.SearchView.type,
            placeholder: .constant((viewModel.type?.selectedType?.rawValue).value),
            icon: Image.Arrows.chevron_right,
            text: .constant(viewModel.type?.selectedType?.rawValue ?? Localizable.SearchView.selectType)
        ) {
            viewModel.shouldShowTypesSheet.toggle()
        }
    }
    
    private var typesList: some View {
        VStack(alignment: .leading) {
            ForEach(SearchItemType.allCases, id: \.self.id) { type in
                Text(type.rawValue)
                    .foregroundColor( viewModel.type?.selectedType == type ? .blue : .black)
                    .padding(.vertical)
                    .onTapGesture {
                        viewModel.shouldShowTypesSheet.toggle()
                        viewModel.type?.selectedType = type
                    } //: onTapGesture
            } //: ForEach
        } //: VStack
        .wrapInHStack(.leading)
    }
}
