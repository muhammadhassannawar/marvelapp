//
//  SearchView.swift
//  MoviesList
//
//  Created by Mohamed Hassan Nawar on 01/06/2022.
//

import SwiftUI

struct SearchView: View {
    // MARK: - ROUTER
    let router: MainRouter = .shared
    
    // MARK: - PROPERTIES
    @StateObject var viewModel: SearchViewModel = .init()
    
    // MARK: - BODY
    var body: some View {
        BaseView(
            state: $viewModel.state,
            alertItem: $viewModel.alertItem
        ) {
            VStack(spacing: Dimensions.SearchView.spacing) {
                navigationView
                contentView
            }
            .frame(maxHeight: .infinity)
            .background(Color.white)
        }
        .overlay(when: viewModel.shouldShowTypesSheet, typesListSheet)
    }
}
