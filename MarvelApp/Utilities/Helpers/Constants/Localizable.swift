//
//  Localizable.swift
//  FootballCompetitions
//
//  Created by Mohamed Hassan Nawar on 05/01/2023.
//

import Foundation

enum Localizable {
    // MARK: - General
    enum General {
        static let okay = "Okay"
        static let error = "Error"
        static let search = "Search"
    }
    
    // MARK: - APIKeys
    enum API {
        static let offset = "offset"
        static let nameStartsWith = "nameStartsWith"
    }
    
    // MARK: - Home
    enum Home {
        static let title = "Home"
        static let seeAll = "See All"
        
        // MARK: - Series
        enum Series {
            static let title = "Series"
            
        }
        
        // MARK: - Events
        enum Events {
            static let title = "Events"
        }
        
        // MARK: - Stories
        enum Stories {
            static let title = "Stories"
            static let story = "Story"
        }
        
        // MARK: - Comics
        enum Comics {
            static let title = "Comics"
        }
        
        // MARK: - Characters
        enum Characters {
            static let title = "Characters"
        }
    }
    
    // MARK: - SearchView
    enum SearchView {
        static let type = "Type"
        static let searchTypes = "Search types"
        static let selectType = "select type"
        static let selectSearchType = "Please select the type of search item"
    }
}
