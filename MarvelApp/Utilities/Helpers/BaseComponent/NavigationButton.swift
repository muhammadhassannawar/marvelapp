//
//  NavigationButton.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 06/01/2023.
//

import SwiftUI

struct NavigationButton: View {
    // MARK: - PROPERTIES
    var action: () -> Void
    var image: Image = Image.Arrows.cross_close
    var renderingMode: Image.TemplateRenderingMode = .original
    
    // MARK: - BODY
    var body: some View {
        Button(action: {
            action()
        }, label: {
            image
                .renderingMode(renderingMode)
        })
    }
}
