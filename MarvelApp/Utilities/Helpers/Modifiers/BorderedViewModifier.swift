//
//  BorderedViewModifier.swift
//  MarvelApp
//
//  Created by Mohamed Hassan Nawar on 06/01/2023.
//

import SwiftUI

struct BorderedViewModifier: ViewModifier {
    // MARK: - BODY
    func body(content: Content) -> some View {
        return content
            .background(Color.white)
            .clipShape(Circle())
            .shadow(color: Color.shadow_color, radius: 6)
    }
}

extension View {
    func bordered() -> some View {
        self.modifier(BorderedViewModifier())
    }
}
